package com.example.ate84d;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.bumptech.glide.request.target.Target;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.storage.FirebaseStorage;

import java.util.ArrayList;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class SearchActivity extends AppCompatActivity {

    private static final String TAG = "com.example.ate84d.SearchActivity";
    public static final String SEARCH = "com.example.ate84d.Search";
    public static final String SETSPECIES = "com.example.ate84d.SetSpecies";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        connectXMLViews();
        connectSearchView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (bottomNav != null) bottomNav.getMenu().findItem(R.id.search_menu_item).setChecked(true);
    }

    private RecyclerViewAdapter adapter;
    private BottomNavigationView bottomNav;

    // Connects to the XML layout file
    private void connectXMLViews() {
        // Connects to the recyclerView and sets it to a new layout manager and adapter
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new RecyclerViewAdapter(this);
        recyclerView.setAdapter(adapter);

        // Sets up the bottom navigation
        bottomNav = findViewById(R.id.bottom_navigation_view);
        new SetupBottomNav().setupListeners(this, bottomNav);
        bottomNav.getMenu().findItem(R.id.search_menu_item).setChecked(true);
    }

    // Connects to the SearchView
    private void connectSearchView() {
        // Connects to the searchView and creates a listener that will alert the recyclerView of text input
        SearchView searchView = findViewById(R.id.searchView);
        TextView queryText = searchView.findViewById(searchView.getContext().getResources()
                .getIdentifier("android:id/search_src_text", null, null));
        queryText.setTypeface(getResources().getFont(R.font.noto_sans));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.v(TAG, "User submitted new search query: " + query);
                adapter.getFilter().filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.v(TAG, "Query text changed: " + newText);
                adapter.getFilter().filter(newText);
                return true;
            }
        });

        // If this activity was started with a predetermined search, execute it
        String search = getIntent().getExtras() == null ? null : getIntent().getExtras().getString(SEARCH);
        if (search != null) {
            queryText.setText(search);
            adapter.getFilter().filter(search);
        }
    }

    // ViewHolder for the search suggestions recyclerView
    private static class RecyclerViewViewHolder extends RecyclerView.ViewHolder {

        private final TextView nameTextView;
        private final TextView infoTextView;
        private final ImageView pictureImageView;
        private final ImageView lightImageView;

        public RecyclerViewViewHolder(@NonNull View itemView) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.search_result_name);
            pictureImageView = itemView.findViewById(R.id.search_result_image);
            infoTextView = itemView.findViewById(R.id.search_result_info);
            lightImageView = itemView.findViewById(R.id.light_icon);
        }
    }

    // Adapter for the search suggestions recyclerView
    private static class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewViewHolder> implements Filterable {

        private final SearchActivity searchActivity;

        // Constructor
        RecyclerViewAdapter(SearchActivity searchActivity) {
            this.searchActivity = searchActivity;
        }

        @NonNull
        @Override
        public RecyclerViewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(searchActivity);
            return new RecyclerViewViewHolder(inflater.inflate(R.layout.item_search_result, parent, false));
        }

        private ArrayList<PlantObject> filteredPlants = PlantsLibrary.getInstance().getPlantObjects();

        @Override
        public void onBindViewHolder(@NonNull RecyclerViewViewHolder holder, int position) {
            PlantObject plantObject = filteredPlants.get(position);
            holder.nameTextView.setText(plantObject.name);
            holder.infoTextView.setText(plantObject.sciName);

            switch (plantObject.light) {
                case "full":
                    holder.lightImageView.setImageResource(R.drawable.full_sun);
                case "part":
                    holder.lightImageView.setImageResource(R.drawable.half_shade);
                case "shade":
                    holder.lightImageView.setImageResource(R.drawable.shade);
                default:
            }

            // Creates a reference to the image file associated with the plant name and loads it into the imageView using Glide
            if (!plantObject.name.equals(PlantObject.DEFAULT_NAME)) {
                GlideApp.with(searchActivity)
                        .load(FirebaseStorage.getInstance().getReferenceFromUrl("gs://ate84d.appspot.com/" + plantObject.name.toLowerCase() + ".jpg"))
                        .placeholder(R.drawable.dewdrop_image)
                        .into(holder.pictureImageView);
            }

            holder.itemView.setOnClickListener(v -> {
                Log.d(TAG, "User selected " + plantObject.name + " from search list");
                Intent intent = new Intent(searchActivity, PlantObjectActivity.class);
                intent.putExtra(PlantObject.PLANT_OBJECT, plantObject);
                searchActivity.startActivity(intent);
            });

            Log.v(TAG, "SuggestionsRecyclerView: holder at position " + position + " set to "
                    + (plantObject.name != null ? plantObject.name : "plant not found"));
        }

        @Override
        public int getItemCount() {
            return filteredPlants.size();
        }

        // Creates a new Filter that will filter and publish results
        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    ArrayList<PlantObject> filteredResults;
                    Log.v(TAG, "New filter for search:" + constraint);
                    if (constraint.length() == 0) { // If the search field is blank, simply show the full library
                        filteredResults = PlantsLibrary.getInstance().getPlantObjects();
                    } else {
                        filteredResults = getFilteredResults(constraint.toString().toLowerCase());
                    }

                    FilterResults results = new FilterResults();
                    results.values = filteredResults;
                    return results;
                }

                @SuppressWarnings("unchecked")
                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    // Sets the results of the filter and notifies the recyclerView
                    filteredPlants = (ArrayList<PlantObject>) results.values;
                    Log.v(TAG, "Filtered plants list changed");
                    notifyDataSetChanged();
                }
            };
        }

        // Filters the plants library with the constraint given
        protected ArrayList<PlantObject> getFilteredResults(String constraint) {
            ArrayList<PlantObject> results = new ArrayList<>();

            for (PlantObject plantObject : PlantsLibrary.getInstance().getPlantObjects()) {
                // Filter for the search; checks if the values to filter are not the default values
                final boolean filter = !plantObject.name.equals(PlantObject.DEFAULT_NAME) && plantObject.name.toLowerCase().contains(constraint)
                        || !plantObject.sciName.equals(PlantObject.DEFAULT_SCI_NAME) && plantObject.sciName.toLowerCase().contains(constraint)
                        || !plantObject.type.equals(PlantObject.DEFAULT_TYPE) && plantObject.type.toLowerCase().contains(constraint)
                        || plantObject.toxic && ("toxic".contains(constraint) || "poisonous".contains(constraint))
                        || !plantObject.bloomTime.equals(PlantObject.DEFAULT_BLOOM) && plantObject.bloomTime.toLowerCase().contains(constraint)
                        || !plantObject.light.equals(PlantObject.DEFAULT_LIGHT) && ("light".contains(constraint) || "shade".contains(constraint))
                        && (plantObject.light.toLowerCase().contains(constraint) || plantObject.light.equals("full") && "sun".contains(constraint));

                if (filter) {
                    results.add(plantObject);
                } else if (plantObject.commonNames != null) { // Checks for the common names list
                    for (String name : plantObject.commonNames) {
                        if (name.toLowerCase().contains(constraint.toLowerCase())) {
                            results.add(plantObject);
                            break;
                        }
                    }
                }
            }

            Log.v(TAG, "Filtered for plant names");
            return results;
        }
    }
}