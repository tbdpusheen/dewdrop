package com.example.ate84d;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class WateringFragment extends Fragment {

    private static final String TAG = "WateringFragment";

    public WateringFragment() {}

    private static final String NUMBER = "Number";
    private static final String TIME_PERIOD = "Time Period";

    public static WateringFragment newInstance(String number, String timePeriod) {
        WateringFragment fragment = new WateringFragment();
        Bundle args = new Bundle();
        args.putString(NUMBER, number);
        args.putString(TIME_PERIOD, timePeriod);
        fragment.setArguments(args);
        return fragment;
    }

    private String number;
    private String timePeriod;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            number = getArguments().getString(NUMBER);
            timePeriod = getArguments().getString(TIME_PERIOD);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_watering, container, false);

        // Sets the droplet info values to their respective parameters
        TextView numberTextView = view.findViewById(R.id.number_text);
        TextView timePeriodTextView = view.findViewById(R.id.time_period_text);

        numberTextView.setText(number.equals("0") ? "" : number);
        timePeriodTextView.setText(number.equals("0") ? "None" : (number.equals("1") || timePeriod.contains("s") ? timePeriod : timePeriod + "s"));

        // Sets the watering info to the combination of both parameters
        String wateringString;
        if (number.equals("0")) {
            wateringString = "Usually doesn't need watering";
        }
        else if (number.equals("1")) {
            wateringString = "Water every " + timePeriod.toLowerCase();
        }
        else {
            wateringString = "Water every " + number + " "  + (timePeriod.contains("s") ? timePeriod : timePeriod + "s");
        }
        TextView wateringTextView = view.findViewById(R.id.watering_text);
        wateringTextView.setText(wateringString);

        Log.v(TAG, "WateringFragment inflated and initialized");
        return view;
    }
}