package com.example.ate84d;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class PlantObjectIconsInfoFragment extends Fragment {

    private static final String TAG = "PlantIconsInfoFragment";

    public PlantObjectIconsInfoFragment() {}

    private static final String PLANT_OBJECT = "PLANT_OBJECT";

    public static PlantObjectIconsInfoFragment newInstance(PlantObject plantObject) {
        PlantObjectIconsInfoFragment fragment = new PlantObjectIconsInfoFragment();
        Bundle args = new Bundle();
        args.putParcelable(PLANT_OBJECT, plantObject);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            PlantObject plantObject = getArguments().getParcelable(PLANT_OBJECT);
            boolean addedToxicityFragment = false;

            // Starts a new transaction for placing fragments inside of this fragment
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();

            // Adds the watering fragment if the plant has a watering amount
            if (!plantObject.waterAmount.equals(PlantObject.DEFAULT_WATER_AMOUNT)) {
                transaction.add(R.id.fragment_container_view, WateringFragment.newInstance(plantObject.waterAmount, plantObject.waterTime.toLowerCase()));
                Log.v(TAG, "LightFragment added to child fragment transaction");

                // As this is the always first transaction if called, also adds the ToxicityFragment
                transaction.add(R.id.fragment_container_view, ToxicityFragment.newInstance(plantObject.toxic));
                addedToxicityFragment = true;
                Log.v(TAG, "ToxicityFragment added to child fragment transaction");
            }
            // Adds the light fragment if the plant has a preferred light
            if (!plantObject.light.equals(PlantObject.DEFAULT_LIGHT)) {
                transaction.add(R.id.fragment_container_view, LightFragment.newInstance(plantObject.light.toLowerCase()));
                Log.v(TAG, "LightFragment added to child fragment transaction");

                // If we haven't created the ToxicityFragment yet, add it now
                if (!addedToxicityFragment) {
                    transaction.add(R.id.fragment_container_view, ToxicityFragment.newInstance(plantObject.toxic));
                    addedToxicityFragment = true;
                    Log.v(TAG, "ToxicityFragment added to child fragment transaction");
                }
            }
            // Adds the bloom fragment if the plant has a bloom season
            if (!plantObject.bloomTime.equals(PlantObject.DEFAULT_BLOOM)) {
                transaction.add(R.id.fragment_container_view, BloomFragment.newInstance(plantObject.bloomTime.toLowerCase()));
                Log.v(TAG, "BloomFragment added to child fragment transaction");

                // If we haven't created the ToxicityFragment yet, add it now
                if (!addedToxicityFragment) {
                    transaction.add(R.id.fragment_container_view, ToxicityFragment.newInstance(plantObject.toxic));
                    addedToxicityFragment = true;
                    Log.v(TAG, "ToxicityFragment added to child fragment transaction");
                }
            }
            // Adds the ToxicityFragment if we haven't created it yet and the PlantObject is toxic
            if (!addedToxicityFragment && plantObject.toxic) {
                transaction.add(R.id.fragment_container_view, ToxicityFragment.newInstance(true));
                Log.v(TAG, "ToxicityFragment added to child fragment transaction");
            }

            transaction.commit();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.v(TAG, "PlantIconsInfoFragment inflated and initialized");
        return inflater.inflate(R.layout.fragment_plant_object_icons_info, container, false);
    }
}