package com.example.ate84d;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class PlantObjectDescriptionFragment extends Fragment {

    public PlantObjectDescriptionFragment() {}

    private static final String DESCRIPTION = "Description";

    public static PlantObjectDescriptionFragment newInstance(String description) {
        PlantObjectDescriptionFragment fragment = new PlantObjectDescriptionFragment();
        Bundle args = new Bundle();
        args.putString(DESCRIPTION, description);
        fragment.setArguments(args);
        return fragment;
    }

    private String description = "Description";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            description = getArguments().getString(DESCRIPTION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_plant_object_description, container, false);
        TextView textView = view.findViewById(R.id.plant_description);
        textView.setText(description);
        return view;
    }
}