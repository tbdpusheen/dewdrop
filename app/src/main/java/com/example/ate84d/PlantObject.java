package com.example.ate84d;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.List;

/**
 * Stores information for a single catalog plant retrieved from {@link com.google.firebase.firestore.FirebaseFirestore}
 */
public class PlantObject implements Parcelable {

    // Variables
    public static final String PLANT_OBJECT = "PLANT_OBJECT";
    public static final String DEFAULT_NAME = "A Lovely Plant";
    public static final String DEFAULT_SCI_NAME = "Scientific Name";
    public static final String DEFAULT_LIGHT = "Light";
    public static final String DEFAULT_LIGHT_TIP = "Tip for light";
    public static final String DEFAULT_SOIL_FERTILIZER = "Soil & Fertilizer";
    public static final String DEFAULT_TYPE = "Type";
    public static final String DEFAULT_WATER_TIP = "Watering";
    public static final String DEFAULT_WATER_AMOUNT = "Number";
    public static final String DEFAULT_WATER_TIME = "Day/Week";
    public static final String DEFAULT_REPOT = "Repotting";
    public static final String DEFAULT_BLOOM = "Blooming season";
    public static final String DEFAULT_DIFFICULTY = "Difficulty";
    public static final String DEFAULT_DESCRIPTION = "Description";

    public String name = DEFAULT_NAME; // Searchable
    public String sciName = DEFAULT_SCI_NAME; // Searchable
    public String light = DEFAULT_LIGHT; // Searchable
    public String lightTip = DEFAULT_LIGHT_TIP;
    public String soilFertilizer = DEFAULT_SOIL_FERTILIZER;
    public String type = DEFAULT_TYPE; // Searchable
    public String waterTip = DEFAULT_WATER_TIP;
    public String waterAmount = DEFAULT_WATER_AMOUNT;
    public String waterTime = DEFAULT_WATER_TIME;
    public String repot = DEFAULT_REPOT;
    public String bloomTime = DEFAULT_BLOOM; // Searchable
    public String difficulty = DEFAULT_DIFFICULTY; // Searchable
    public String description = DEFAULT_DESCRIPTION;
    public List<String> commonNames; // Searchable
    public boolean toxic; // Searchable

    // Empty constructor used by Firebase
    @SuppressWarnings("unused")
    public PlantObject() {}

    @NonNull
    @Override
    public String toString() {
        return "PlantObject{" +
                "name='" + name + '\'' +
                ", sciName='" + sciName + '\'' +
                ", prefLight='" + light + '\'' +
                ", lightTip='" + lightTip + '\'' +
                ", soilFertilizer='" + soilFertilizer + '\'' +
                ", type='" + type + '\'' +
                ", waterTip='" + waterTip + '\'' +
                ", waterAmount='" + waterAmount + '\'' +
                ", waterTime='" + waterTime + '\'' +
                ", repot='" + repot + '\'' +
                ", bloomTime='" + bloomTime + '\'' +
                ", difficulty='" + difficulty + '\'' +
                ", description='" + description + '\'' +
                ", commonNames=" + commonNames +
                ", toxic=" + toxic +
                '}';
    }

    //region Parcelable implementation
    protected PlantObject(Parcel in) {
        name = in.readString();
        sciName = in.readString();
        light = in.readString();
        lightTip = in.readString();
        soilFertilizer = in.readString();
        type = in.readString();
        waterTip = in.readString();
        waterAmount = in.readString();
        waterTime = in.readString();
        repot = in.readString();
        bloomTime = in.readString();
        difficulty = in.readString();
        description = in.readString();
        commonNames = in.createStringArrayList();
        toxic = in.readByte() != 0;
    }

    public static final Creator<PlantObject> CREATOR = new Creator<PlantObject>() {
        @Override
        public PlantObject createFromParcel(Parcel in) {
            return new PlantObject(in);
        }

        @Override
        public PlantObject[] newArray(int size) {
            return new PlantObject[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(sciName);
        dest.writeString(light);
        dest.writeString(lightTip);
        dest.writeString(soilFertilizer);
        dest.writeString(type);
        dest.writeString(waterTip);
        dest.writeString(waterAmount);
        dest.writeString(waterTime);
        dest.writeString(repot);
        dest.writeString(bloomTime);
        dest.writeString(difficulty);
        dest.writeString(description);
        dest.writeStringList(commonNames);
        dest.writeByte((byte) (toxic ? 1 : 0));
    }
    //endregion
}
