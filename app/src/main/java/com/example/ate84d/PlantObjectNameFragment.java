package com.example.ate84d;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.storage.FirebaseStorage;

public class PlantObjectNameFragment extends Fragment {

    public PlantObjectNameFragment() {}

    // Creates a new instance of the fragment with the supplied arguments
    public static PlantObjectNameFragment newInstance(PlantObject plantObject) {
        PlantObjectNameFragment fragment = new PlantObjectNameFragment();
        Bundle args = new Bundle();
        args.putParcelable(PlantObject.PLANT_OBJECT, plantObject);
        fragment.setArguments(args);
        return fragment;
    }

    private PlantObject plantObject;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            plantObject = getArguments().getParcelable(PlantObject.PLANT_OBJECT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_plant_object_name, container, false);

        ImageButton plantButton = view.findViewById(R.id.add_to_garden_button);
        plantButton.setOnClickListener(v -> {
            // Adds the plant to FirebaseFirestore
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if (user != null) {
                Plant plant = new Plant();
                FirebaseFirestore.getInstance().collection(user.getUid())
                        .add(plant)
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful() && task.getResult() != null) {
                                plant.fireStoreID = task.getResult().getId();
                                FirebaseFirestore.getInstance().collection(user.getUid())
                                        .document(plant.fireStoreID)
                                        .set(plant, SetOptions.merge());

                                Intent intent = new Intent(requireContext(), EditPlantActivity.class);
                                intent.putExtra(PlantObject.PLANT_OBJECT, plantObject);
                                intent.putExtra(Plant.PLANT, plant);
                                startActivity(intent);
                            }
                        });
            }
        });

        TextView nameView = view.findViewById(R.id.plant_name);
        nameView.setText(plantObject.name);
        TextView sciNameView = view.findViewById(R.id.plant_sci_name);
        sciNameView.setText(!plantObject.sciName.equals(PlantObject.DEFAULT_SCI_NAME)
                ? plantObject.sciName : "");

        GlideApp.with(this)
                .load(FirebaseStorage.getInstance().getReferenceFromUrl(
                        "gs://ate84d.appspot.com/" + plantObject.name.toLowerCase() + ".jpg"))
                .placeholder(R.drawable.dewdrop_image)
                .into((ImageView) view.findViewById(R.id.plant_image));

        return view;
    }
}