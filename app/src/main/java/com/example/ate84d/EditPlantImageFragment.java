package com.example.ate84d;

import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.errorprone.annotations.CanIgnoreReturnValue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class EditPlantImageFragment extends Fragment {

    public EditPlantImageFragment() {}

    private static final String PLANT_ID = "PlantId";
    private static final String TAG = "EditPlantImageFragment";

    public static EditPlantImageFragment newInstance(String plantId) {
        EditPlantImageFragment fragment = new EditPlantImageFragment();
        Bundle args = new Bundle();
        args.putString(PLANT_ID, plantId);
        fragment.setArguments(args);
        return fragment;
    }

    private String plantId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            plantId = getArguments().getString(PLANT_ID);
        }
    }

    private Uri uri;
    private final ActivityResultLauncher<Uri> launcher = registerForActivityResult(
            new ActivityResultContracts.TakePicture(), result -> {
        if (result && getView() != null) {
            GlideApp.with(requireContext())
                    .load(new File(requireActivity().getExternalFilesDir(
                            Environment.DIRECTORY_PICTURES), plantId + ".jpg"))
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .placeholder(R.drawable.dewdrop_image)
                    .into((ImageView) getView().findViewById(R.id.plant_image));
        }
    });

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_plant_image, container, false);

        if (requireActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
            File file = new File(requireActivity().getExternalFilesDir(
                    Environment.DIRECTORY_PICTURES), plantId + ".jpg");
            uri = FileProvider.getUriForFile(requireContext(),
                    "com.example.ate84d.fileprovider", file);
            // Retrieves the image of the plant that the user previously saved and loads it using Glide
            GlideApp.with(requireContext())
                    .load(file)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .placeholder(R.drawable.dewdrop_image)
                    .into((ImageView) view.findViewById(R.id.plant_image));

            // Sets an onClickListener that will allow the user to add an image
            view.setOnClickListener(v -> {
                try {
                    if (!file.exists() || (file.exists() && file.delete())) {
                        if (file.createNewFile()) {
                            Log.d(TAG, "New file created");
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                launcher.launch(uri);
            });
        } else {
            Toast.makeText(requireContext(), "No camera detected", Toast.LENGTH_SHORT).show();
        }

        return view;
    }
}