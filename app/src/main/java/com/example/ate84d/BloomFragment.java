package com.example.ate84d;

import android.content.res.ColorStateList;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Objects;

public class BloomFragment extends Fragment {

    private static final String TAG = "BloomFragment";

    public BloomFragment() {}

    private static final String BLOOM = "Bloom";

    public static BloomFragment newInstance(String bloom) {
        BloomFragment fragment = new BloomFragment();
        Bundle args = new Bundle();
        args.putString(BLOOM, bloom);
        fragment.setArguments(args);
        return fragment;
    }

    private String bloom;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            bloom = getArguments().getString(BLOOM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bloom, container, false);

        ImageView imageView = view.findViewById(R.id.bloom_icon);
        TextView textView = view.findViewById(R.id.bloom_text);
        String text;

        // The default is Spring, so no need to change anything if it's only Spring
        if (bloom.contains("spring")) {
            if (bloom.contains("fall") || bloom.contains("autumn")) {
                imageView.setImageResource(R.drawable.spring_fall);
                imageView.setImageTintList(null);
                text = "Blooms in Spring & Fall";
                textView.setText(text);
            } else if (bloom.contains("summer")) {
                imageView.setImageResource(R.drawable.spring_summer);
                imageView.setImageTintList(null);
                text = "Blooms in Spring & Summer";
                textView.setText(text);
            }
        } else if (bloom.contains("summer")) {
            if (bloom.contains("fall") || bloom.contains("autumn")) {
                imageView.setImageResource(R.drawable.summer_fall);
                imageView.setImageTintList(null);
                text = "Blooms in Summer & Fall";
                textView.setText(text);
            }
            imageView.setImageTintList(ContextCompat.getColorStateList(requireActivity(), R.color.yellow));
            text = "Blooms in Summer";
            textView.setText(text);
        } else if (bloom.contains("fall")) {
            imageView.setImageTintList(ContextCompat.getColorStateList(requireActivity(), R.color.orange));
            text = "Blooms in Fall";
            textView.setText(text);
        }

        return view;
    }
}