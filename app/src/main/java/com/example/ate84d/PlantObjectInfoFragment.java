package com.example.ate84d;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class PlantObjectInfoFragment extends Fragment {

    public PlantObjectInfoFragment() {}

    private static final String HEADING = "Heading";
    private static final String INFO = "Information";

    public static PlantObjectInfoFragment newInstance(String heading, String info) {
        PlantObjectInfoFragment fragment = new PlantObjectInfoFragment();
        Bundle args = new Bundle();
        args.putString(HEADING, heading);
        args.putString(INFO, info);
        fragment.setArguments(args);
        return fragment;
    }

    private String heading;
    private String info;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            heading = getArguments().getString(HEADING);
            info = getArguments().getString(INFO);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_plant_object_info, container, false);

        TextView headingTextView = view.findViewById(R.id.heading_text);
        headingTextView.setText(heading);
        TextView infoTextView = view.findViewById(R.id.info_text);
        infoTextView.setText(info);

        return view;
    }
}