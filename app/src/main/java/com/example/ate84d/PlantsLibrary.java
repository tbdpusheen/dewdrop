package com.example.ate84d;

import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import org.jetbrains.annotations.Contract;

import java.util.ArrayList;

/**
 * Stores the ArrayList of {@link PlantObject} retrieved from {@link FirebaseFirestore}
 *
 * <P>Also stores the ArrayList of {@link Plant} corresponding to the current {@link FirebaseAuth} user
 */
public class PlantsLibrary {

    // Variables
    private static final String TAG = "PlantsLibrary";
    private volatile static PlantsLibrary INSTANCE;
    private static final ArrayList<PlantObject> plantObjects = new ArrayList<>();
    private static ArrayList<Plant> plants = new ArrayList<>();
    private static boolean hasRetrievedPlantObjects = false;
    private static boolean hasRetrievedPlants = false;

    // Constructor
    private PlantsLibrary() {}

    /**
     * Returns an instance of the {@link PlantsLibrary}
     *
     * <p>If this is the first time an instance has been called for, instantiates the singleton
     * and retrieves from {@link FirebaseFirestore}
     */
    public synchronized static PlantsLibrary getInstance() {
        if(INSTANCE == null) {
            // Lazily instantiates the instance
            INSTANCE = new PlantsLibrary();
            retrieveFromFirestore();
        }

        return INSTANCE;
    }

    // Retrieves data from cloud firestore
    private static void retrieveFromFirestore() {
        // Retrieves the plantObjects from the Cloud Firestore "plants" collection
        FirebaseFirestore.getInstance().collection("plants")
            .get()
            .addOnCompleteListener(task -> {
                if (task.isSuccessful() && task.getResult() != null) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        plantObjects.add(document.toObject(PlantObject.class));
                    }
                    plantObjects.sort((o1, o2) -> o1.name.compareToIgnoreCase(o2.name));
                    hasRetrievedPlantObjects = true;
                } else Log.e(TAG, "Error getting plants: ", task.getException());
            });

        plants = PlantsLibrary.getInstance().updatePlants();
    }

    /**
     * Returns an updated ArrayList of {@link Plant} retrieved from {@link FirebaseAuth#getCurrentUser()},
     * using the {@link FirebaseAuth#getCurrentUser()}
     */
    public ArrayList<Plant> updatePlants() {
        hasRetrievedPlants = false;
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            FirebaseFirestore.getInstance().collection(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful() && task.getResult() != null) {
                            plants = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                plants.add(document.toObject(Plant.class));
                            }
                            plants.sort(((o1, o2) -> o1.name.compareToIgnoreCase(o2.name)));
                            hasRetrievedPlants = true;
                        }
                    });
        }
        return plants;
    }

    /**
     * Returns the ArrayList of {@link Plant} retrieved from {@link FirebaseFirestore},
     * using the {@link FirebaseAuth#getCurrentUser()}
     * */
    public ArrayList<Plant> getPlants() {
        return plants;
    }

    /** Returns the ArrayList of {@link PlantObject} retrieved from {@link FirebaseFirestore} */
    public ArrayList<PlantObject> getPlantObjects() {
        return plantObjects;
    }

    /**
     * Returns whether both the {@link ArrayList} of {@link PlantObject} and {@link Plant} has finished
     * retrieving from {@link FirebaseFirestore}
     */
    @Contract(pure = true)
    public static boolean isFinishedRetrieving() {
        return hasRetrievedPlantObjects && hasRetrievedPlants;
    }
}
