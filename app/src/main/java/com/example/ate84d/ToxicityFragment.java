package com.example.ate84d;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class ToxicityFragment extends Fragment {

    private static final String TOXIC = "Toxic";
    private static final String TAG = "ToxicityFragment";

    private boolean toxic;

    public ToxicityFragment() {}

    public static ToxicityFragment newInstance(boolean toxic) {
        ToxicityFragment fragment = new ToxicityFragment();
        Bundle args = new Bundle();
        args.putBoolean(TOXIC, toxic);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            toxic = getArguments().getBoolean(TOXIC);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_toxicity, container, false);

        if (!toxic) {
            ImageView imageView = view.findViewById(R.id.toxicity_icon);
            imageView.setImageResource(R.drawable.non_toxic);
            TextView textView = view.findViewById(R.id.toxicity_text);
            textView.setText(getText(R.string.non_toxic_string));
        }

        Log.v(TAG, "ToxicityFragment inflated and initialized");
        return view;
    }
}