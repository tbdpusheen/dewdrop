package com.example.ate84d;

import android.os.Bundle;

import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.Target;

import java.io.File;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class PlantNameFragment extends Fragment {

    public PlantNameFragment() {}

    public static PlantNameFragment newInstance(Plant plant) {
        PlantNameFragment fragment = new PlantNameFragment();
        Bundle args = new Bundle();
        args.putParcelable(Plant.PLANT, plant);
        fragment.setArguments(args);
        return fragment;
    }

    private Plant plant;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            plant = getArguments().getParcelable(Plant.PLANT);
            if (plant.plantObject != null) {
                requireActivity().getSupportFragmentManager().beginTransaction()
                        .add(R.id.plant_species_container, SpeciesFragment.newInstance(plant.plantObject))
                        .commit();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_plant_name, container, false);

        TextView plantNameText = view.findViewById(R.id.plant_name);
        plantNameText.setText(plant.name);
        TextView plantLocationText = view.findViewById(R.id.plant_location);
        plantLocationText.setText(plant.location);

        TextView waterAmountText = view.findViewById(R.id.water_amount_text);
        TextView waterTimeText = view.findViewById(R.id.water_time_text);
        String string;
        int days = plant.calculateWatering();
        if (days <= 0) {
            waterAmountText.setVisibility(View.INVISIBLE);
            string = "Today";
            waterTimeText.setText(string);
        } else if (days >= 7) {
            waterAmountText.setText(String.valueOf(days/7));
            string = days >= 14 ? "weeks" : "week";
            waterTimeText.setText(string);
        } else {
            waterAmountText.setText(String.valueOf(days));
        }

        // If the file for the plant image exists, loads it using Glide
        GlideApp.with(requireContext())
                .load(new File(requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                                plant.fireStoreID + ".jpg"))
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .placeholder(R.drawable.dewdrop_image)
                .into((ImageView) view.findViewById(R.id.plant_image));

        return view;
    }
}