package com.example.ate84d;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class PlantActivity extends AppCompatActivity {

    private Plant plant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plant);

        // Retrieves the plant that started this activity
        if (getIntent().getParcelableExtra(Plant.PLANT) != null) {
            plant = getIntent().getParcelableExtra(Plant.PLANT);
        }

        connectXMLViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (bottomNav != null) {
            bottomNav.getMenu().findItem(R.id.garden_menu_item).setChecked(true);
        }
        // If there was any old fragments, clear those
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
        }
        initializeFragments();
    }

    BottomNavigationView bottomNav;

    // Connects the XML views
    private void connectXMLViews() {
        // Sets up the bottom navigation
        bottomNav = findViewById(R.id.bottom_navigation_view);
        new SetupBottomNav().setupListeners(this, bottomNav);
        bottomNav.getMenu().findItem(R.id.garden_menu_item).setChecked(true);

        // Sets up the edit button to edits plants
        FloatingActionButton editButton = findViewById(R.id.edit_button);
        editButton.setOnClickListener(v -> {
            Intent intent = new Intent(this, EditPlantActivity.class);
            intent.putExtra(Plant.PLANT, plant);
            startActivity(intent);
        });
    }

    // Initializes fragments
    private void initializeFragments() {
        // Begins a new transaction
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fragment_container_view, PlantNameFragment.newInstance(plant));
        if (!plant.notes.equals(Plant.DEFAULT_NOTES)) {
            transaction.add(R.id.fragment_container_view, PlantInfoFragment.newInstance(
                    "Notes", plant.notes));
        }
        if (!plant.waterTip.equals(Plant.DEFAULT_WATER_TIP)) {
            transaction.add(R.id.fragment_container_view, PlantInfoFragment.newInstance(
                    "Watering Tip", plant.waterTip));
        }
        if (!plant.lightTip.equals(Plant.DEFAULT_LIGHT_TIP)) {
            transaction.add(R.id.fragment_container_view, PlantInfoFragment.newInstance(
                    "Light", plant.lightTip));
        }
        if (!plant.soilFertilizer.equals(Plant.DEFAULT_SOIL_FERTILIZER)) {
            transaction.add(R.id.fragment_container_view, PlantInfoFragment.newInstance(
                    "Soil & Fertilizer", plant.soilFertilizer));
        }
        if (!plant.repot.equals(Plant.DEFAULT_REPOT)) {
            transaction.add(R.id.fragment_container_view, PlantInfoFragment.newInstance(
                    "Repotting", plant.repot));
        }
        transaction.add(R.id.fragment_container_view, PaddingFragment.newInstance(250)).commit();
    }
}