package com.example.ate84d;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

public class EditPlantInfoFragment extends Fragment {

    public EditPlantInfoFragment() {}

    private static final String HEADING = "Heading";
    private static final String INFO = "Info";
    private static final String HINT = "Hint";

    public static EditPlantInfoFragment newInstance(String heading, String info, String hint) {
        EditPlantInfoFragment fragment = new EditPlantInfoFragment();
        Bundle args = new Bundle();
        args.putString(HEADING, heading);
        args.putString(INFO, info);
        args.putString(HINT, hint);
        fragment.setArguments(args);
        return fragment;
    }

    private String heading;
    private String info;
    private String hint;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            heading = getArguments().getString(HEADING);
            info = getArguments().getString(INFO);
            hint = getArguments().getString(HINT);
        }
    }

    private EditText infoTextView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_plant_info, container, false);

        TextView headingTextView = view.findViewById(R.id.heading_text);
        headingTextView.setText(heading);
        infoTextView = view.findViewById(R.id.info_text);
        if (info != null) {
            infoTextView.setText(info);
        }
        infoTextView.setHint(hint);

        return view;
    }

    /** Returns the info that this fragment contains */
    public String getInfo() {
        return !infoTextView.getText().toString().equals("") ? infoTextView.getText().toString() : null;
    }
}