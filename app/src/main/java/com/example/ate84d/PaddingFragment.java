package com.example.ate84d;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class PaddingFragment extends Fragment {

    private static final String HEIGHT = "Height";

    private int height;

    public PaddingFragment() {}

    public static PaddingFragment newInstance(int height) {
        PaddingFragment fragment = new PaddingFragment();
        Bundle args = new Bundle();
        args.putInt(HEIGHT, height);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            height = getArguments().getInt(HEIGHT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_padding, container, false);
        view.setPadding(0, height, 0, 0);
        return view;
    }
}