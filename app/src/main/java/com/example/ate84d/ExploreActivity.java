package com.example.ate84d;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.request.target.Target;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.storage.FirebaseStorage;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class ExploreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explore);

        connectXMLViews();
    }

    private BottomNavigationView bottomNav;

    @Override
    protected void onResume() {
        super.onResume();
        if (bottomNav != null) {
            bottomNav.getMenu().findItem(R.id.search_menu_item).setChecked(true);
        }
    }

    // Connects XML Views
    private void connectXMLViews() {
        // Sets up a pseudo-search box which directs the user to the search activity
        findViewById(R.id.search_button).setOnClickListener(v -> startActivity(
                new Intent(this, SearchActivity.class)));

        bottomNav = findViewById(R.id.bottom_navigation_view);
        new SetupBottomNav().setupListeners(this, bottomNav);
        bottomNav.getMenu().findItem(R.id.search_menu_item).setChecked(true);

        // Sets the categories recyclerView to a vertical grid layout with two columns and a new adapter
        RecyclerView categoriesRecyclerView = findViewById(R.id.categories_recyclerView);
        categoriesRecyclerView.setLayoutManager(new GridLayoutManager(
                this, 2, RecyclerView.VERTICAL, false));
        categoriesRecyclerView.setAdapter(new CategoriesRecyclerViewAdapter(this, generateCategories()));
    }

    // Generates a random ArrayList<String> of categories using random PlantObjects and their type
    private ArrayList<String> generateCategories() {
        ArrayList<PlantObject> plantObjects = PlantsLibrary.getInstance().getPlantObjects();
        ArrayList<String> categories = new ArrayList<>();
        int forceStopLooping = 0;

        for (int i = 0; i < 14; i++) {
            Collections.shuffle(plantObjects);
            PlantObject plantObject = plantObjects.get(0);
            // If the plantObject has a type and the categories ArrayList doesn't already include the type, add it
            if (!plantObject.type.equals(PlantObject.DEFAULT_TYPE)
                    && !categories.contains(plantObject.type.contains(" ")
                    ? StringUtils.capitalize(plantObject.type.substring(0, plantObject.type.indexOf(" ")))
                    : StringUtils.capitalize(plantObject.type))) {

                categories.add(plantObject.type.contains(" ")
                        ? StringUtils.capitalize(plantObject.type.substring(0, plantObject.type.indexOf(" ")))
                        : StringUtils.capitalize(plantObject.type));
            } else {
                i--;
            }

            // How many times to try to get categories until we stop
            if (forceStopLooping++ > 100) {
                break;
            }
        }
        return categories;
    }

    // ViewHolder for the categories recyclerView
    private static class CategoriesRecyclerViewHolder extends RecyclerView.ViewHolder {

        public CategoriesRecyclerViewHolder(@NonNull View itemView) {
            super(itemView);
            backgroundImageView = itemView.findViewById(R.id.background_image);
            categoryNameTextView = itemView.findViewById(R.id.category_name);
        }
        private final ImageView backgroundImageView;
        private final TextView categoryNameTextView;
    }

    // Adapter for the categories recyclerView
    private static class CategoriesRecyclerViewAdapter extends RecyclerView.Adapter<CategoriesRecyclerViewHolder> {

        private final Context context;
        private final ArrayList<String> plantObjects;

        CategoriesRecyclerViewAdapter(Context context, ArrayList<String> plantObjects) {
            this.context = context;
            this.plantObjects = plantObjects;
        }

        @NonNull
        @Override
        public CategoriesRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(context);
            return new CategoriesRecyclerViewHolder(inflater.inflate(
                    R.layout.item_explore_category, parent, false));
        }

        private final FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();

        @Override
        public void onBindViewHolder(@NonNull CategoriesRecyclerViewHolder holder, int position) {
            String category = plantObjects.get(position);
            holder.categoryNameTextView.setText(category);

            // Finds a plant that fits the category
            for (PlantObject plantObject : PlantsLibrary.getInstance().getPlantObjects()) {
                if (plantObject.type.toLowerCase().contains(category.toLowerCase())) {
                    // Creates a reference to the image file associated with the plant name and loads it into the imageView using Glide
                    GlideApp.with(context)
                            .load(firebaseStorage.getReferenceFromUrl(
                                    "gs://ate84d.appspot.com/" + plantObject.name.toLowerCase() + ".jpg"))
                            .placeholder(R.drawable.dewdrop_image)
                            .into(holder.backgroundImageView);
                    break;
                }
            }

            // Starts the search activity with the selected category
            holder.itemView.setOnClickListener(v -> {
                Intent intent = new Intent(context, SearchActivity.class);
                intent.putExtra(SearchActivity.SEARCH, category);
                context.startActivity(intent);
            });
        }

        @Override
        public int getItemCount() {
            return plantObjects.size();
        }
    }
}