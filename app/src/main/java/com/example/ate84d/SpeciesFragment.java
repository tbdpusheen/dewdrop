package com.example.ate84d;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.firebase.storage.FirebaseStorage;

public class SpeciesFragment extends Fragment {


    public SpeciesFragment() {}

    private PlantObject plantObject;

    public static SpeciesFragment newInstance(PlantObject plantObject) {
        SpeciesFragment fragment = new SpeciesFragment();
        Bundle args = new Bundle();
        args.putParcelable(PlantObject.PLANT_OBJECT, plantObject);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            plantObject = getArguments().getParcelable(PlantObject.PLANT_OBJECT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_species, container, false);

        GlideApp.with(requireContext())
                .load(FirebaseStorage.getInstance().getReferenceFromUrl("gs://ate84d.appspot.com/" + plantObject.name.toLowerCase() + ".jpg"))
                .placeholder(R.drawable.dewdrop_image)
                .into((ImageView) view.findViewById(R.id.plant_image));

        return view;
    }
}