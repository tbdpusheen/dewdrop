package com.example.ate84d;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class GardenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_garden);

        connectXMLViews();
        setupAddPlantButton();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (bottomNav != null) {
            bottomNav.getMenu().findItem(R.id.garden_menu_item).setChecked(true);
        }

        if (adapter != null) {
            // Waits for the PlantsLibrary to finishing retrieving the user's plants
            ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
            executorService.scheduleAtFixedRate(() -> runOnUiThread(() -> {
                if (PlantsLibrary.isFinishedRetrieving()) {
                    adapter.plants = PlantsLibrary.getInstance().getPlants();
                    adapter.notifyDataSetChanged();
                    if (adapter.getItemCount() == 0) {
                        noPlantsTextView.setVisibility(View.VISIBLE);
                    } else {
                        noPlantsTextView.setVisibility(View.INVISIBLE);
                    }
                    executorService.shutdown();
                }
            }), 0, 100, TimeUnit.MILLISECONDS);
        }
    }

    private PlantsAdapter adapter;
    private BottomNavigationView bottomNav;
    private TextView noPlantsTextView;

    // Connects the XML views
    private void connectXMLViews() {
        // Sets up the bottom navigation
        bottomNav = findViewById(R.id.bottom_navigation_view);
        new SetupBottomNav().setupListeners(this, bottomNav);
        bottomNav.getMenu().findItem(R.id.garden_menu_item).setChecked(true);

        // Sets up the plants recyclerView using the custom classes
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        ArrayList<Plant> plants = PlantsLibrary.getInstance().getPlants();
        adapter = new PlantsAdapter(this, plants);
        recyclerView.setAdapter(adapter);

        noPlantsTextView = findViewById(R.id.no_plant_text);
        if (adapter.getItemCount() == 0) {
            noPlantsTextView.setVisibility(View.VISIBLE);
        }
    }

    // Sets up the add plant button
    private void setupAddPlantButton() {
        FloatingActionButton addPlantButton = findViewById(R.id.add_plant_button);
        addPlantButton.setOnClickListener(v -> {
            // Adds the plant to FirebaseFirestore
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if (user != null) {
                Plant plant = new Plant();
                FirebaseFirestore.getInstance().collection(user.getUid())
                        .add(plant)
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful() && task.getResult() != null) {
                                plant.fireStoreID = task.getResult().getId();
                                FirebaseFirestore.getInstance().collection(user.getUid())
                                        .document(plant.fireStoreID)
                                        .set(plant, SetOptions.merge());

                                Intent intent = new Intent(this, EditPlantActivity.class);
                                intent.putExtra(Plant.PLANT, plant);
                                startActivity(intent);
                            }
                        });
            }
        });
    }

    // ViewHolder
    private static class PlantsViewHolder extends RecyclerView.ViewHolder {

        private final ImageView plantImageView;
        private final ImageView wateringImageView;
        private final TextView plantNameTextView;
        private final TextView plantLocationTextView;
        private final TextView wateringAmountTextView;
        private final TextView wateringTimeTextView;

        public PlantsViewHolder(@NonNull View itemView) {
            super(itemView);
            this.plantImageView = itemView.findViewById(R.id.plant_image);
            this.wateringImageView = itemView.findViewById(R.id.watering_icon);
            this.plantNameTextView = itemView.findViewById(R.id.plant_name);
            this.plantLocationTextView = itemView.findViewById(R.id.plant_location);
            this.wateringAmountTextView = itemView.findViewById(R.id.number_text);
            this.wateringTimeTextView = itemView.findViewById(R.id.time_period_text);
        }
    }

    // Adapter
    private class PlantsAdapter extends RecyclerView.Adapter<PlantsViewHolder> {

        private final Context context;
        private ArrayList<Plant> plants;

        PlantsAdapter(Context context, ArrayList<Plant> plants) {
            this.context = context;
            this.plants = plants;
        }

        @NonNull
        @Override
        public PlantsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(context);
            return new PlantsViewHolder(inflater.inflate(
                    R.layout.item_garden_plant, parent, false));
        }

        boolean finishedRetrieval = false;

        @Override
        public void onBindViewHolder(@NonNull PlantsViewHolder holder, int position) {
            Plant plant = plants.get(position);

            holder.plantNameTextView.setText(plant.name);
            holder.plantLocationTextView.setText(plant.location);

            updateWatering(holder, plant);

            // If the file for the plant image exists, loads it using Glide
            GlideApp.with(context)
                    .load(new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                                    plant.fireStoreID + ".jpg"))
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .placeholder(R.drawable.dewdrop_image)
                    .into(holder.plantImageView);

            // Sets up the onClickListener to water plants
            holder.wateringImageView.setOnClickListener(v -> {
                Calendar lastWatered = Calendar.getInstance();
                lastWatered.setTimeInMillis(plant.dateLastWater);
                if (lastWatered.get(Calendar.DAY_OF_YEAR) == Calendar.getInstance().get(Calendar.DAY_OF_YEAR)
                        && lastWatered.get(Calendar.YEAR) == Calendar.getInstance().get(Calendar.YEAR)) {
                    Snackbar.make(holder.wateringImageView, "You've already watered this today!",
                            BaseTransientBottomBar.LENGTH_SHORT).show();
                } else {
                    long previousDate = plant.dateLastWater;
                    plant.dateLastWater = Calendar.getInstance().getTimeInMillis();
                    updateWatering(holder, plant);

                    // Merges the plant to FirebaseFirestore
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    if (user != null) {
                        FirebaseFirestore.getInstance().collection(user.getUid())
                                .document(plant.fireStoreID)
                                .set(plant, SetOptions.merge());

                        PlantsLibrary.getInstance().updatePlants();
                        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
                        executorService.scheduleAtFixedRate(() -> runOnUiThread(() -> {
                            if (PlantsLibrary.isFinishedRetrieving() && !finishedRetrieval) {
                                finishedRetrieval = true;
                                plants = PlantsLibrary.getInstance().getPlants();
                                adapter.notifyItemChanged(position);
                                executorService.shutdown();
                            }
                        }), 0, 100, TimeUnit.MILLISECONDS);
                    }

                    Snackbar.make(holder.wateringImageView, plant.name + " was watered.", Snackbar.LENGTH_SHORT)
                            .setAction("UNDO", v1 -> {
                                plant.dateLastWater = previousDate;
                                updateWatering(holder, plant);

                                // Merges the plant to FirebaseFirestore
                                if (user != null) {
                                    FirebaseFirestore.getInstance().collection(user.getUid())
                                            .document(plant.fireStoreID)
                                            .set(plant, SetOptions.merge());

                                    finishedRetrieval = false;
                                    PlantsLibrary.getInstance().updatePlants();
                                    ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
                                    executorService.scheduleAtFixedRate(() -> runOnUiThread(() -> {
                                        if (PlantsLibrary.isFinishedRetrieving() && !finishedRetrieval) {
                                            finishedRetrieval = true;
                                            plants = PlantsLibrary.getInstance().getPlants();
                                            adapter.notifyItemChanged(position);
                                            executorService.shutdown();
                                        }
                                    }), 0, 100, TimeUnit.MILLISECONDS);
                                }
                            }).show();
                }

            });

            // Sets up the onClickListener to go to the plant
            holder.itemView.setOnClickListener(v -> {
                Intent intent = new Intent(context, PlantActivity.class);
                intent.putExtra(Plant.PLANT, plant);
                context.startActivity(intent);
            });
        }

        // Sets the views to the plant.calculateWatering() result
        private void updateWatering(@NonNull PlantsViewHolder holder, Plant plant) {
            int newDays = plant.calculateWatering();
            String newString;
            if (newDays <= 0) {
                holder.wateringAmountTextView.setVisibility(View.INVISIBLE);
                newString = "Today";
                holder.wateringTimeTextView.setText(newString);
            } else if (newDays >= 7) {
                holder.wateringAmountTextView.setText(String.valueOf(newDays / 7));
                newString = newDays >= 14 ? "weeks" : "week";
                holder.wateringTimeTextView.setText(newString);
            } else {
                holder.wateringAmountTextView.setText(String.valueOf(newDays));
                newString = newDays != 1 ? "days" : "day";
                holder.wateringTimeTextView.setText(newString);
            }
        }

        @Override
        public int getItemCount() {
            return plants.size();
        }
    }
}