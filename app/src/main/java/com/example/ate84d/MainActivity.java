package com.example.ate84d;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private boolean finishedRetrieval = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Signs in the user as an anonymous user
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            FirebaseAuth.getInstance().signInAnonymously()
                    .addOnCompleteListener(task -> {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInAnonymously:failure", task.getException());
                            Toast.makeText(this,
                                    "Authentication failed. Please make sure you have a stable internet connection.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
        }

        // If the PlantsLibrary hasn't finished retrieving the library yet, wait for it
        if (PlantsLibrary.isFinishedRetrieving()) {
            setContentView(R.layout.activity_main);
            connectXMLViews();
        } else {
            setContentView(R.layout.activity_splash);
            PlantsLibrary.getInstance();
            ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
            executorService.scheduleAtFixedRate(() -> runOnUiThread(() -> {
                if (PlantsLibrary.isFinishedRetrieving() && !finishedRetrieval) {
                    finishedRetrieval = true;
                    setContentView(R.layout.activity_main);
                    connectXMLViews();
                    executorService.shutdown();
                }
            }), 1, 1, TimeUnit.SECONDS);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (bottomNav != null) {
            bottomNav.getMenu().findItem(R.id.home_menu_item).setChecked(true);
        }
    }

    private BottomNavigationView bottomNav;
    private TextView noPlantsText;
    private RecyclerView wateringTodayRecyclerView;
    private final Calendar calendar = Calendar.getInstance();

    // Connects the XML views
    private void connectXMLViews() {
        noPlantsText = findViewById(R.id.no_plant_text);

        // Instantiates the bottom navigation using the SetupBottomNavigation class
        bottomNav = findViewById(R.id.bottom_navigation_view);
        new SetupBottomNav().setupListeners(this, bottomNav);
        bottomNav.getMenu().findItem(R.id.home_menu_item).setChecked(true);

        // Creates a new calendar based on the current time
        updateCalendar();

        wateringTodayRecyclerView = findViewById(R.id.watering_today_recyclerView);
        wateringTodayRecyclerView.setLayoutManager(new LinearLayoutManager(this,
                RecyclerView.HORIZONTAL, false));

        // Listeners for updating the calendar
        ImageButton previousMonthButton = findViewById(R.id.previous_month_button);
        previousMonthButton.setOnClickListener(v -> {
            calendar.add(Calendar.MONTH, -1);
            wateringTodayRecyclerView.setAdapter(new WaterTodayAdapter(
                    this, new ArrayList<>(), Calendar.getInstance()));
            updateCalendar();
        });
        ImageButton nextMonthButton = findViewById(R.id.next_month_button);
        nextMonthButton.setOnClickListener(v -> {
            calendar.add(Calendar.MONTH, 1);
            wateringTodayRecyclerView.setAdapter(new WaterTodayAdapter(
                    this, new ArrayList<>(), Calendar.getInstance()));
            updateCalendar();
        });
    }

    // Creates a new calendar based on the month input
    private void updateCalendar() {
        ArrayList<Date> days = new ArrayList<>();
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);

        // Sets the title textView to the desired month & year
        TextView calendarMonthYear = findViewById(R.id.month_year_textView);
        calendarMonthYear.setText(new SimpleDateFormat("MMMM yyyy", Locale.CANADA)
                .format(calendar.getTime()));

        // Sets the day to the first day of the current month
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        // Starts the calendar at the first monday before our current date
        calendar.add(Calendar.DAY_OF_MONTH, -(calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY
                ? calendar.get(Calendar.DAY_OF_WEEK) : 8) + Calendar.MONDAY);

        // Keeps adding dates until the calendar is filled
        while (days.size() < 42) {
            days.add(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        // Sets up the calendar recyclerView
        RecyclerView recyclerView = findViewById(R.id.calendar_recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 7));
        recyclerView.setAdapter(new CalendarAdapter(this, days, month, recyclerView));

        // Reset the calendar to what it was before we created the recyclerView
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.MONTH, month);
    }

    // ViewHolder for the watering today recyclerView
    private static class WaterTodayViewHolder extends RecyclerView.ViewHolder {

        private final ImageView plantImage;
        private final ImageView wateredImage;

        public WaterTodayViewHolder(@NonNull View itemView) {
            super(itemView);
            plantImage = itemView.findViewById(R.id.plant_image);
            wateredImage = itemView.findViewById(R.id.watered_icon);
        }
    }

    // Adapter for the watering today recyclerView
    private static class WaterTodayAdapter extends RecyclerView.Adapter<WaterTodayViewHolder> {

        private final MainActivity mainActivity;
        private final ArrayList<Plant> plants;
        private final Calendar calendar;

        WaterTodayAdapter(MainActivity mainActivity, ArrayList<Plant> plants, Calendar calendar) {
            this.mainActivity = mainActivity;
            this.plants = plants;
            this.calendar = calendar;

            if (plants.size() == 0) {
                mainActivity.noPlantsText.setVisibility(View.VISIBLE);
            } else {
                mainActivity.noPlantsText.setVisibility(View.INVISIBLE);
            }
        }

        @NonNull
        @Override
        public WaterTodayViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(mainActivity);
            return new WaterTodayViewHolder(inflater.inflate(R.layout.item_water_today, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull WaterTodayViewHolder holder, int position) {
            Plant plant = plants.get(position);

            GlideApp.with(mainActivity)
                    .load(new File(mainActivity.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                            plant.fireStoreID + ".jpg"))
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .placeholder(R.drawable.dewdrop_image)
                    .into(holder.plantImage);

            Calendar lastWatered = Calendar.getInstance();
            lastWatered.setTimeInMillis(plant.dateLastWater);
            if (calendar.get(Calendar.DAY_OF_YEAR) == lastWatered.get(Calendar.DAY_OF_YEAR)
                    && calendar.get(Calendar.YEAR) == lastWatered.get(Calendar.YEAR)) {
                holder.plantImage.setImageTintList(ColorStateList.valueOf(
                        mainActivity.getColor(R.color.trans_black_2)));
                holder.wateredImage.setImageResource(R.drawable.filled_circle);
            } else {
                holder.plantImage.setImageTintList(null);
                holder.wateredImage.setImageResource(R.drawable.empty_circle);
            }

            holder.plantImage.setOnClickListener(v -> {
                if (lastWatered.get(Calendar.DAY_OF_YEAR) == Calendar.getInstance().get(Calendar.DAY_OF_YEAR)
                        && lastWatered.get(Calendar.YEAR) == Calendar.getInstance().get(Calendar.YEAR)) {
                    Snackbar.make(holder.plantImage, "You've already watered this today!",
                            BaseTransientBottomBar.LENGTH_SHORT).show();
                } else {
                    long previousDate = plant.dateLastWater;
                    plant.dateLastWater = Calendar.getInstance().getTimeInMillis();
                    holder.plantImage.setImageTintList(ColorStateList.valueOf(
                            mainActivity.getColor(R.color.trans_black_2)));
                    holder.wateredImage.setImageResource(R.drawable.filled_circle);

                    // Merges the plant to FirebaseFirestore
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    if (user != null) {
                        FirebaseFirestore.getInstance().collection(user.getUid())
                                .document(plant.fireStoreID)
                                .set(plant, SetOptions.merge());

                        PlantsLibrary.getInstance().updatePlants();
                        super.notifyDataSetChanged();
                    }

                    Snackbar.make(holder.plantImage, plant.name + " was watered.", Snackbar.LENGTH_SHORT)
                            .setAction("UNDO", v1 -> {
                                plant.dateLastWater = previousDate;
                                holder.plantImage.setImageTintList(null);
                                holder.wateredImage.setImageResource(R.drawable.empty_circle);

                                // Merges the plant to FirebaseFirestore
                                if (user != null) {
                                    FirebaseFirestore.getInstance().collection(user.getUid())
                                            .document(plant.fireStoreID)
                                            .set(plant, SetOptions.merge());

                                    PlantsLibrary.getInstance().updatePlants();
                                    super.notifyDataSetChanged();
                                }
                            }).show();
                }
            });
        }

        @Override
        public int getItemCount() {
            return plants.size();
        }
    }

    // ViewHolder for the calendar
    private static class CalendarViewHolder extends RecyclerView.ViewHolder {

        public CalendarViewHolder(@NonNull View itemView) {
            super(itemView);
            this.dateTextView = itemView.findViewById(R.id.date);
            this.dateUnderline = itemView.findViewById(R.id.date_underline);
            this.selectedDateBorder = itemView.findViewById(R.id.selected_border);
        }

        private final TextView dateTextView;
        private final View dateUnderline;
        private final ImageView selectedDateBorder;
    }

    // Adapter for the calendar
    private static class CalendarAdapter extends RecyclerView.Adapter<CalendarViewHolder> {

        private final MainActivity mainActivity;
        private final int month;
        private final ArrayList<Date> days;
        private final RecyclerView recyclerView;

        CalendarAdapter(MainActivity mainActivity, ArrayList<Date> days, int month, RecyclerView recyclerView) {
            this.mainActivity = mainActivity;
            this.days = days;
            this.month = month;
            this.recyclerView = recyclerView;
        }

        @NonNull
        @Override
        public CalendarViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(mainActivity);
            return new CalendarViewHolder(inflater.inflate(R.layout.item_calendar, parent, false));
        }

        private int selectedDatePosition = -1;

        @Override
        public void onBindViewHolder(@NonNull CalendarViewHolder holder, int position) {
            Calendar today = Calendar.getInstance();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(days.get(position));

            // Sets the text to the formatted day in the date
            String date = "" + calendar.get(Calendar.DAY_OF_MONTH);
            holder.dateTextView.setText(date);

            // Checks to see the date of this holder is inside of the current month
            if (calendar.get(Calendar.MONTH) == month) {
                ArrayList<Plant> plantsOnDate = new ArrayList<>();
                for (Plant plant : PlantsLibrary.getInstance().getPlants()) {
                    Calendar wateringDate = Calendar.getInstance();
                    wateringDate.setTimeInMillis(plant.dateLastWater);
                    wateringDate.set(Calendar.HOUR_OF_DAY, 0);
                    wateringDate.set(Calendar.MINUTE, 0);
                    wateringDate.set(Calendar.MILLISECOND, 0);
                    while (wateringDate.before(calendar)) {
                        if (wateringDate.get(Calendar.DAY_OF_YEAR) == calendar.get(Calendar.DAY_OF_YEAR)
                                && wateringDate.get(Calendar.YEAR) == calendar.get(Calendar.YEAR)) {
                            plantsOnDate.add(plant);
                        }
                        wateringDate.add(Calendar.DATE, plant.waterAmount);
                    }
                }
                switch (plantsOnDate.size()) {
                    case 0: break;
                    case 1:
                        holder.itemView.setBackgroundColor(mainActivity.getColor(R.color.blue3));
                        break;
                    case 2:
                        holder.itemView.setBackgroundColor(mainActivity.getColor(R.color.blue2));
                        break;
                    default:
                        holder.itemView.setBackgroundColor(mainActivity.getColor(R.color.blue1));
                }

                // Checks to see if the date is the current date
                if (calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)
                        && calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR)) {
                    // If it is, make the underline visible
                    selectedDatePosition = position;
                    holder.dateUnderline.setVisibility(View.VISIBLE);
                    holder.selectedDateBorder.setVisibility(View.VISIBLE);

                    // Sets the watering today recyclerView to the plants on the selected date
                    mainActivity.wateringTodayRecyclerView.setAdapter(new WaterTodayAdapter(
                            mainActivity, plantsOnDate, calendar));
                }
                holder.itemView.setOnClickListener(v -> {
                    if (selectedDatePosition != -1) {
                        CalendarViewHolder viewHolder = (CalendarViewHolder)
                                recyclerView.findViewHolderForAdapterPosition(selectedDatePosition);
                        assert viewHolder != null;
                        viewHolder.selectedDateBorder.setVisibility(View.INVISIBLE);
                    }
                    holder.selectedDateBorder.setVisibility(View.VISIBLE);
                    mainActivity.wateringTodayRecyclerView.setAdapter(new WaterTodayAdapter(
                            mainActivity, plantsOnDate, calendar));

                    selectedDatePosition = position;
                });
            } else {
                holder.dateTextView.setTextColor(mainActivity.getColor(R.color.grey_2));
            }
        }

        @Override
        public int getItemCount() {
            return days.size();
        }
    }
}