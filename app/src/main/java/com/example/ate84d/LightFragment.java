package com.example.ate84d;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class LightFragment extends Fragment {

    private static final String TAG = "LightFragment";
    private static final String LIGHT = "Light";

    public LightFragment() {}

    public static LightFragment newInstance(String light) {
        LightFragment fragment = new LightFragment();
        Bundle args = new Bundle();
        args.putString(LIGHT, light);
        fragment.setArguments(args);
        return fragment;
    }

    private String light;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            light = getArguments().getString(LIGHT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_light, container, false);

        ImageView lightImageView = view.findViewById(R.id.light_icon);
        TextView lightTextView = view.findViewById(R.id.light_text);
        switch (light) {
            case "full":
                lightImageView.setImageResource(R.drawable.full_sun);
                lightTextView.setText(R.string.full_light_string);
            case "part":
                lightImageView.setImageResource(R.drawable.half_shade);
                lightTextView.setText(R.string.half_light_string);
            case "shade":
                lightImageView.setImageResource(R.drawable.shade);
                lightTextView.setText(R.string.shade_light_string);
            default:
        }

        Log.v(TAG, "LightFragment inflated and initialized");
        return view;
    }
}