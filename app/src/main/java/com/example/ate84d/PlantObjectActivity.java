package com.example.ate84d;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.util.Log;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class PlantObjectActivity extends AppCompatActivity {

    public static final String TAG = "PlantObjectActivity";
    private PlantObject plantObject;
    private BottomNavigationView bottomNav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plant_object);

        // Gets the plant passed on by the previous activity
        plantObject = getIntent().getParcelableExtra(PlantObject.PLANT_OBJECT);

        // Sets up the bottom navigation
        bottomNav = findViewById(R.id.bottom_navigation_view);
        new SetupBottomNav().setupListeners(this, bottomNav);
        bottomNav.getMenu().findItem(R.id.search_menu_item).setChecked(true);

        initializeFragments();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (bottomNav != null) {
            bottomNav.getMenu().findItem(R.id.search_menu_item).setChecked(true);
        }
    }

    // Starts a new fragment transaction which will add all necessary fragments
    private void initializeFragments() {
        // Begins a new transaction
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fragment_container_view, PlantObjectNameFragment.newInstance(plantObject));

        // Adds the description fragment if the plant has a description
        if (!plantObject.description.equals(PlantObject.DEFAULT_DESCRIPTION)) {
            transaction.add(R.id.fragment_container_view, PlantObjectDescriptionFragment.newInstance(plantObject.description));
        }
        // Adds the icons fragment if the plant has any parameters with icons
        if (!plantObject.light.equals(PlantObject.DEFAULT_LIGHT)
                || !plantObject.waterAmount.equals(PlantObject.DEFAULT_WATER_AMOUNT)
                || !plantObject.bloomTime.equals(PlantObject.DEFAULT_BLOOM)
                || plantObject.toxic) {
            // Starts a new child transaction to add all the icons we need
            transaction.add(R.id.fragment_container_view, PlantObjectIconsInfoFragment.newInstance(plantObject));
        }
        // Adds respective info fragments if the plants has them
        if (!plantObject.waterTip.equals(PlantObject.DEFAULT_WATER_TIP)) {
            transaction.add(R.id.fragment_container_view, PlantObjectInfoFragment.newInstance(PlantObject.DEFAULT_WATER_TIP, plantObject.waterTip));
        }
        if (!plantObject.soilFertilizer.equals(PlantObject.DEFAULT_SOIL_FERTILIZER)) {
            transaction.add(R.id.fragment_container_view, PlantObjectInfoFragment.newInstance(PlantObject.DEFAULT_SOIL_FERTILIZER, plantObject.soilFertilizer));
        }
        if (!plantObject.repot.equals(PlantObject.DEFAULT_REPOT)) {
            transaction.add(R.id.fragment_container_view, PlantObjectInfoFragment.newInstance(PlantObject.DEFAULT_REPOT, plantObject.repot));
        }

        transaction.commit();
    }
}