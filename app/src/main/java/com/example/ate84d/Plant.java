package com.example.ate84d;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.Calendar;

/**
 * Stores information for a single Plant that the user has in their garden
 */
public class Plant implements Parcelable {

    // Variables
    public static final String PLANT = "PLANT";
    public static final String DEFAULT_NAME = "My Plant";
    public static final String DEFAULT_LOCATION = "Home";
    public static final int DEFAULT_WATER_AMOUNT = 2;
    public static final String DEFAULT_REPOT = "Repotting";
    public static final String DEFAULT_WATER_TIP = "Watering";
    public static final String DEFAULT_LIGHT_TIP = "Tip for light";
    public static final String DEFAULT_NOTES = "Notes";
    public static final String DEFAULT_SOIL_FERTILIZER = "Soil & Fertilizer";

    public PlantObject plantObject;
    public String fireStoreID;
    public String notes = DEFAULT_NOTES;
    public String name = DEFAULT_NAME;
    public String location = DEFAULT_LOCATION;
    public String lightTip = DEFAULT_LIGHT_TIP;
    public String repot = DEFAULT_REPOT;
    public String waterTip = DEFAULT_WATER_TIP;
    public String soilFertilizer = DEFAULT_SOIL_FERTILIZER;
    public int waterAmount = DEFAULT_WATER_AMOUNT;
    public long dateLastWater;

    public Plant() {}

    //region Parcelable Implementation
    protected Plant(Parcel in) {
        plantObject = in.readParcelable(PlantObject.class.getClassLoader());
        fireStoreID = in.readString();
        notes = in.readString();
        name = in.readString();
        location = in.readString();
        lightTip = in.readString();
        repot = in.readString();
        waterTip = in.readString();
        soilFertilizer = in.readString();
        waterAmount = in.readInt();
        dateLastWater = in.readLong();
    }

    public static final Creator<Plant> CREATOR = new Creator<Plant>() {
        @Override
        public Plant createFromParcel(Parcel in) {
            return new Plant(in);
        }

        @Override
        public Plant[] newArray(int size) {
            return new Plant[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(plantObject, flags);
        dest.writeString(fireStoreID);
        dest.writeString(notes);
        dest.writeString(name);
        dest.writeString(location);
        dest.writeString(lightTip);
        dest.writeString(repot);
        dest.writeString(waterTip);
        dest.writeString(soilFertilizer);
        dest.writeInt(waterAmount);
        dest.writeLong(dateLastWater);
    }
    //endregion

    @NonNull
    @Override
    public String toString() {
        return "Plant{" +
                "plantObject=" + plantObject +
                ", fireStoreID='" + fireStoreID + '\'' +
                ", name='" + name + '\'' +
                ", location='" + location + '\'' +
                ", waterAmount='" + waterAmount + '\'' +
                ", lightTip='" + lightTip + '\'' +
                ", repot='" + repot + '\'' +
                ", waterTip='" + waterTip + '\'' +
                ", soilFertilizer='" + soilFertilizer + '\'' +
                ", dateLastWater=" + dateLastWater +
                '}';
    }

    /**
     * Returns an int representing how many days there are until the plant needs watering
     */
    public int calculateWatering() {
        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 1); // Offset so that lastWater is always behind
        today.set(Calendar.MILLISECOND, 0);
        Calendar lastWater = Calendar.getInstance();
        lastWater.setTimeInMillis(dateLastWater);
        lastWater.set(Calendar.HOUR_OF_DAY, 0);
        lastWater.set(Calendar.MINUTE, 0);
        lastWater.set(Calendar.MILLISECOND, 0);

        int days = -1;
        while (lastWater.before(today)) {
            lastWater.add(Calendar.DATE, 1);
            ++days;
        }

        return waterAmount - days;
    }
}
