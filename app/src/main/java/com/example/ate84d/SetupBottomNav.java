package com.example.ate84d;

import android.content.Context;
import android.content.Intent;

import com.google.android.material.bottomnavigation.BottomNavigationView;

/**
 * Class containing a simple setup method for a {@link BottomNavigationView}
 *
 * @see SetupBottomNav#setupListeners(Context, BottomNavigationView)
 */
public class SetupBottomNav {

    /**
     * Sets up the {@link BottomNavigationView#setOnNavigationItemSelectedListener}
     *
     * @param context The {@link Context} to use during setup
     * @param view The {@link BottomNavigationView} to setup
     */
    public void setupListeners(Context context, BottomNavigationView view) {
        view.setOnNavigationItemSelectedListener(item -> {
            if (item.getItemId() == R.id.home_menu_item) {
                context.startActivity(new Intent(context, MainActivity.class));
                return true;
            } else if (item.getItemId() == R.id.garden_menu_item) {
                context.startActivity(new Intent(context, GardenActivity.class));
                return true;
            } else if (item.getItemId() == R.id.search_menu_item) {
                context.startActivity(new Intent(context, ExploreActivity.class));
                return true;
            }
            return false;
        });
    }
}
