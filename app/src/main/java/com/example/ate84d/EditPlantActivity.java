package com.example.ate84d;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class EditPlantActivity extends AppCompatActivity {

    private Plant plant;
    private boolean initialized = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_plant);

        // Gets the plant from the intent that started this activity
        if (getIntent().getParcelableExtra(Plant.PLANT) != null) {
            plant = getIntent().getParcelableExtra(Plant.PLANT);
            if (plant.dateLastWater == 0L) {
                plant.dateLastWater = Calendar.getInstance().getTimeInMillis();
            }

            if (getIntent().getParcelableExtra(PlantObject.PLANT_OBJECT) != null) {
                PlantObject plantObject = getIntent().getParcelableExtra(PlantObject.PLANT_OBJECT);
                plant.plantObject = plantObject;
                if (!plantObject.name.equals(PlantObject.DEFAULT_NAME)) {
                    plant.name = plantObject.name;
                }
                if (!plantObject.waterAmount.equals(PlantObject.DEFAULT_WATER_AMOUNT)) {
                    try {
                        if (plantObject.waterTime.toLowerCase().equals("week")
                                || plantObject.waterTime.toLowerCase().equals("weeks")) {
                            plant.waterAmount = Integer.parseInt(plantObject.waterAmount) * 7;
                        } else {
                            plant.waterAmount = Integer.parseInt(plantObject.waterAmount);
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
                if (!plantObject.waterTip.equals(PlantObject.DEFAULT_WATER_TIP)) {
                    plant.waterTip = plantObject.waterTip;
                }
                if (!plantObject.lightTip.equals(PlantObject.DEFAULT_LIGHT_TIP)) {
                    plant.lightTip = plantObject.lightTip;
                }
                if (!plantObject.soilFertilizer.equals(PlantObject.DEFAULT_SOIL_FERTILIZER)) {
                    plant.soilFertilizer = plantObject.soilFertilizer;
                }
                if (!plantObject.repot.equals(PlantObject.DEFAULT_REPOT)) {
                    plant.repot = plantObject.repot;
                }
            }

            // If the plant doesn't exist yet, wait for it
            ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
            executorService.scheduleAtFixedRate(() -> runOnUiThread(() -> {
                if (plant.fireStoreID != null && !initialized) {
                    initialized = true;

                    initializeFragments();
                    setupDeleteButton();
                    setupDoneButton();

                    // Sets up the bottom navigation
                    BottomNavigationView bottomNav = findViewById(R.id.bottom_navigation_view);
                    new SetupBottomNav().setupListeners(this, bottomNav);
                    bottomNav.getMenu().findItem(R.id.garden_menu_item).setChecked(true);

                    executorService.shutdown();
                }
            }), 0, 100, TimeUnit.MILLISECONDS);
        }
    }

    // Sets up the delete button
    private void setupDeleteButton() {
        FloatingActionButton deleteButton = findViewById(R.id.delete_button);
        deleteButton.setVisibility(View.VISIBLE);
        deleteButton.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.delete_plant_string)
                    .setMessage("Are you sure you want to permanently delete " + plant.name + "?")
                    .setPositiveButton(R.string.yes_string, (dialog, which) -> {
                        // Deletes the plant from FirebaseFirestore
                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        if (user != null) {
                            FirebaseFirestore.getInstance().collection(user.getUid())
                                    .document(plant.fireStoreID)
                                    .delete();
                            Toast.makeText(this, "Plant deleted", Toast.LENGTH_SHORT).show();
                            PlantsLibrary.getInstance().updatePlants();

                            startActivity(new Intent(this, GardenActivity.class));
                            finish();
                        }
                    })
                    .setNegativeButton(R.string.no_string, ((dialog, which) -> {}))
                    .show();
        });
    }

    // Sets up the done button
    private void setupDoneButton() {
        // Sets up the done button
        FloatingActionButton doneButton = findViewById(R.id.done_button);
        doneButton.setOnClickListener(v -> {
            EditPlantNameFragment nameFragment = (EditPlantNameFragment)
                    getSupportFragmentManager().findFragmentByTag(EditPlantNameFragment.TAG);
            assert nameFragment != null;
            plant.name = nameFragment.getName().trim();
            plant.location = nameFragment.getLocation().trim();
            plant.waterAmount = Integer.parseInt(nameFragment.getWaterAmount().trim());

            EditPlantInfoFragment infoFragment = (EditPlantInfoFragment)
                    getSupportFragmentManager().findFragmentByTag(Plant.DEFAULT_NOTES);
            if (infoFragment != null && infoFragment.getInfo() != null) {
                plant.notes = infoFragment.getInfo().trim();
            }
            infoFragment = (EditPlantInfoFragment)
                    getSupportFragmentManager().findFragmentByTag(Plant.DEFAULT_LIGHT_TIP);
            if (infoFragment != null && infoFragment.getInfo() != null) {
                plant.lightTip = infoFragment.getInfo().trim();
            }
            infoFragment = (EditPlantInfoFragment)
                    getSupportFragmentManager().findFragmentByTag(Plant.DEFAULT_WATER_TIP);
            if (infoFragment != null && infoFragment.getInfo() != null) {
                plant.waterTip = infoFragment.getInfo().trim();
            }
            infoFragment = (EditPlantInfoFragment)
                    getSupportFragmentManager().findFragmentByTag(Plant.DEFAULT_REPOT);
            if (infoFragment != null && infoFragment.getInfo() != null) {
                plant.repot = infoFragment.getInfo().trim();
            }
            infoFragment = (EditPlantInfoFragment)
                    getSupportFragmentManager().findFragmentByTag(Plant.DEFAULT_SOIL_FERTILIZER);
            if (infoFragment != null && infoFragment.getInfo() != null) {
                plant.soilFertilizer = infoFragment.getInfo().trim();
            }

            // Adds the plant to FirebaseFirestore
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if (user != null) {
                FirebaseFirestore.getInstance().collection(user.getUid())
                        .document(plant.fireStoreID)
                        .set(plant, SetOptions.merge());

                Toast.makeText(this, "Plant saved", Toast.LENGTH_SHORT).show();
                PlantsLibrary.getInstance().updatePlants();
            }

            startActivity(new Intent(this, GardenActivity.class));
            finish();
        });
    }

    // Initializes fragments
    private void initializeFragments() {
        // Begins a new transaction
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fragment_container_view, EditPlantImageFragment.newInstance(plant.fireStoreID))
                .add(R.id.fragment_container_view, EditPlantNameFragment.newInstance(plant),
                EditPlantNameFragment.TAG)
                .add(R.id.fragment_container_view, EditPlantInfoFragment.newInstance("Notes",
                        plant.notes.equals(Plant.DEFAULT_NOTES) ? null : plant.notes,
                        "Add some notes!"), Plant.DEFAULT_NOTES)
                .add(R.id.fragment_container_view, EditPlantInfoFragment.newInstance("Watering Tip",
                        plant.waterTip.equals(Plant.DEFAULT_WATER_TIP) ? null : plant.waterTip,
                        "Add some watering notes!"), Plant.DEFAULT_WATER_TIP)
                .add(R.id.fragment_container_view, EditPlantInfoFragment.newInstance("Light",
                        plant.lightTip.equals(Plant.DEFAULT_LIGHT_TIP) ? null : plant.lightTip,
                        "Add some tips for lighting!"), Plant.DEFAULT_LIGHT_TIP)
                .add(R.id.fragment_container_view, EditPlantInfoFragment.newInstance("Soil & Fertilizer",
                        plant.soilFertilizer.equals(Plant.DEFAULT_SOIL_FERTILIZER) ? null : plant.soilFertilizer,
                        "Add some tips for the soil and fertilizing!"), Plant.DEFAULT_SOIL_FERTILIZER)
                .add(R.id.fragment_container_view, EditPlantInfoFragment.newInstance("Repotting",
                        plant.repot.equals(Plant.DEFAULT_REPOT) ? null : plant.repot,
                        "Add some notes on repotting your plant!"), Plant.DEFAULT_REPOT)
                .add(R.id.fragment_container_view, PaddingFragment.newInstance(250))
                .commit();
    }
}