package com.example.ate84d;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class EditPlantNameFragment extends Fragment {

    public static final String TAG = "EditPlantNameFragment";

    public EditPlantNameFragment() {}

    public static EditPlantNameFragment newInstance(Plant plant) {
        EditPlantNameFragment fragment = new EditPlantNameFragment();
        Bundle args = new Bundle();
        args.putParcelable(Plant.PLANT, plant);
        fragment.setArguments(args);
        return fragment;
    }

    private Plant plant;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            plant = getArguments().getParcelable(Plant.PLANT);
        }
    }

    private EditText plantNameEditText;
    private EditText plantLocationEditText;
    private EditText wateringAmountEditText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_plant_name, container, false);

        // Sets the text to the plant information
        plantNameEditText = view.findViewById(R.id.plant_name);
        plantNameEditText.setText(plant.name);
        plantLocationEditText = view.findViewById(R.id.plant_location);
        plantLocationEditText.setText(plant.location);
        wateringAmountEditText = view.findViewById(R.id.water_amount_text);
        wateringAmountEditText.setText(String.valueOf(plant.waterAmount));

        return view;
    }

    // Public methods to retrieve information
    public String getName() {
        return !plantNameEditText.getText().toString().equals("")
                ? plantNameEditText.getText().toString() : Plant.DEFAULT_NAME;
    }

    public String getLocation() {
        return !plantLocationEditText.getText().toString().equals("")
                ? plantLocationEditText.getText().toString() : Plant.DEFAULT_LOCATION;
    }

    public String getWaterAmount() {
        return !wateringAmountEditText.getText().toString().equals("")
                ? wateringAmountEditText.getText().toString() : String.valueOf(Plant.DEFAULT_WATER_AMOUNT);
    }
}