dewdrop
=
Team Members
--
Bastien Wu - Developer

Angel Hsieh - Developer

Isabelle Qiu - Developer

Colin Chen - Designer

Vikrant Anandkumar - Designer

Who We Are
--
Around The Earth in 84 Days (ATE84D) was created with a goal to help users achieve what they want and to help make the earth a better place for everyone, all in a span of 84 days. We decided to create dewdrop as a watering and management app so that people can have an easier time taking care of and loving their plants. Caring for a vitalizing and growing environment in your home can be a lot of work, but the benefits for yourself and for the earth can be monumental. With the help of **dewdrop**, anyone can help *Make Earth Great Again* by putting love and care into their own home ecosystem.

Contact Bastien for access to the plants/user database if needed.